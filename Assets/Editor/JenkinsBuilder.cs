﻿using UnityEditor;
using System;
using System.Collections.Generic;

class JenkinsBuilder {

	static string[] SCENES = FindEnabledEditorScenes ();

	// TODO -NEED CHANGE
	static string APP_NAME = "JenkinsBuilder"; // Application name
	static string TARGET_DIR = "/Users/ariyalawanitchanon/Desktop"; // Must be full path
	//END NEED CHANGE

	[MenuItem ("Jenkins Builder/Build Android")]
	static void PerformAndroidBuild ()
	{
		string target_dir = APP_NAME + ".apk";
		GenericBuild (SCENES, TARGET_DIR + "/" + target_dir, BuildTarget.Android, BuildOptions.None);
	}

	[MenuItem ("Jenkins Builder/Build iOS")]
	static void PerformiOSBuild ()
	{
		string target_dir = APP_NAME;
		GenericBuild (SCENES, TARGET_DIR + "/" + target_dir, BuildTarget.iOS, BuildOptions.None);
	}

	private static string[] FindEnabledEditorScenes()
	{
		List<string> EditorScenes = new List<string> ();
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			if (!scene.enabled)
				continue;
			EditorScenes.Add (scene.path);
		}

		return EditorScenes.ToArray ();
	}

	static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget (build_target);
		string res = BuildPipeline.BuildPlayer (scenes, target_dir, build_target, build_options);
		if (res.Length > 0) {
			throw new Exception ("BuildPlayer failure: " + res);
		}
	}
}
